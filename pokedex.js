var start = 1;
var pageSize = 10;
fetchList(`https://pokedex-api.bitslab.cl/api/?start=${start}&count=${pageSize}`);
loadPokemon(1);
document.getElementById("less").onclick = function(){
    hiddePokemonDetails();
};

function fetchList(url) {
    fetch(url)
        .then(response => response.json())
        .then(data => fillgrid(data));
}

function fillgrid(json) {
    var table = document.getElementById('pokedex-table');
    table.innerHTML = "";
    var last = 0;
    for (var i = 0; i < json.pokemonList.length; i++) {
        tr = table.insertRow(-1);
        var tabCell = tr.insertCell(-1);
        last = json.pokemonList[i].id;
        tabCell.innerHTML = "<a href='#' onclick='loadPokemon(" + last + ");'>" + last + "</a>";
        var tabCell = tr.insertCell(-1);
        tabCell.innerHTML = json.pokemonList[i].name;
    }
    var prev = json.pokemonList[0].id > 1;
    var next = last < json.total;
    var down = json.pokemonList[0].id - pageSize;
    var anteriorURL = `https://pokedex-api.bitslab.cl/api/?start=${down}&count=${pageSize}`;

    var up = json.pokemonList[0].id + pageSize;
    var proximoURL = `https://pokedex-api.bitslab.cl/api/?start=${up}&count=${pageSize}`;

    if(prev) {
        document.getElementById("atras").onclick = function(){fetchList(anteriorURL)};
        document.getElementById("atras").removeAttribute("hidden");
    } else document.getElementById("atras").setAttribute("hidden","true");
    
    if(next) {
        document.getElementById("adelante").onclick = function(){fetchList(proximoURL)};
        document.getElementById("adelante").removeAttribute("hidden");
    } else document.getElementById("adelante").setAttribute("hidden","true");
}

function loadPokemon(id) {
    var img = document.getElementById("pokeImage");
    img.src = `https://pokedex-api.bitslab.cl/api/${id}/picture`;
    fetch(`https://pokedex-api.bitslab.cl/api/${id}`)
        .then(response => response.json())
        .then(data => fillResume(data));
}

function loadPokemonDetails(id) {
    var img = document.getElementById("pokeImage");
    img.src = `https://pokedex-api.bitslab.cl/api/${id}/picture`;
    fetch(`https://pokedex-api.bitslab.cl/api/${id}/details`)
        .then(response => response.json())
        .then(data => fillDetails(data));
}

function fillResume(data) {
    hiddePokemonDetails();
    document.getElementById("pokeName").innerText = data.name;
    document.getElementById("more").onclick = function(){loadPokemonDetails(data.id);};
    document.getElementById("pokeWeight").innerText = data.weight;
    var abilities = "";
    data.abilities.forEach(element => abilities += element.ability + " ");
    document.getElementById("pokeAbilities").innerText = abilities;
    var types = "";
    data.types.forEach(element => types += element.type + " ");
    document.getElementById("pokeTypes").innerText = types;
    hiddePokemonDetails();
}

function fillDetails(data) {
    fillResume(data);
    var evolutions = "<span class='material-icons text-dark'>egg</span>";
    data.evolutions.forEach(element => {
        evolutions += "<span class='material-icons text-dark'>redo</span>"
        evolutions += "<a href='#' onclick='loadPokemon(" + element.id + ");'>"+element.name+"</a>" + " "
    });
    document.getElementById("pokeEvolutions").innerHTML = evolutions;
    showPokemonDetails();
}

function hiddePokemonDetails(){
    document.getElementById("evolRow").setAttribute("hidden","true");
    document.getElementById("more").removeAttribute("hidden");
    document.getElementById("less").setAttribute("hidden",true);
}

function showPokemonDetails(){
    document.getElementById("evolRow").removeAttribute("hidden");
    document.getElementById("less").removeAttribute("hidden");
    document.getElementById("more").setAttribute("hidden",true);
}
