# pokedex-demo

Una aplicación HTML extremadamente simple para demostrar el uso de la [API de Pokedex](https://pokedex-api.bitslab.cl/)

# Como usar

La aplicación presenta una página web en la que al lado derecho se muestra una lista de Pokemones. Si se selecciona uno se puede ver su ficha.

En la ficha se puede presionar sobre el signo + para cargar más información (sus evoluciones).

Puede probar el ejempo en https://enmala.gitlab.io/pokedex-demo

# Como esta contruida

La aplicación solamente utiliza Javascript puro y Bootstrap para organizar la información.
Los datos son obtenidos desde la API desarrollada en este otro [proyecto](https://gitlab.com/enmala/pokedex-api).

# Como se publica

La aplicación se publica utilizando gitlab pages directamente desde el repositorio.
Para ver cómo, revise el archivo de configuración [CI/CD de Gitlab](.gitlab-cl.yml) para este proyecto.




